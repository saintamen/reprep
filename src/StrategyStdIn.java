import java.util.Scanner;

/**
 * Created by amen on 8/21/17.
 */
public class StrategyStdIn implements IInputStrategy {

    private Scanner sc = new Scanner(System.in);
    public StrategyStdIn() {
    }
//dlaczego
    @Override
    public int getInt() {
        return sc.nextInt();
    }

    @Override
    public double getDouble() {
        return sc.nextDouble();
    }

    @Override
    public String getString() {
        return sc.next();
    }
}
