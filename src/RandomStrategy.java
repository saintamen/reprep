import java.util.Random;

/**
 * Created by amen on 8/21/17.
 */
public class RandomStrategy implements IInputStrategy {


    public RandomStrategy() {
    }

    @Override
    public String getString() {
        return "Pozdro389";
    }

    @Override
    public double getDouble() {
        Random random = new Random();
        return random.nextDouble();
    }

    @Override
    public int getInt() {
        Random random = new Random();
        return random.nextInt();
    }
}
